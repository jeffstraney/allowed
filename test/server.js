const crypto = require('crypto');

const express = require('express'),
      session = require('express-session'),
      app = express();

const allowed = require('index')();

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use(session({
  secret: crypto.randomBytes(64).toString('hex'),
  resave: false,
  saveUninitialized: false
}));

// final results
const results = {};

const log_result = (req, res) => {

  const path = req.path;

  // should be a user object
  if (!req.session || !req.session.current_user)
    return res.json({message:'somehow request got through for ' + path});

  const current_user = req.session.current_user;

  const {name} = current_user;

  // see if user was supposed to get to here
  const supposed_to_pass = current_user.cases[path];

  results[name] = results[name] || {};

  // they were supposed to pass, the test case works. If they weren't
  // and got to this middleware, the allowed middleware failed.
  results[name][path] = supposed_to_pass;

  const response = {};

  response[name + ':' + path] = results[name][path];

  res.json(response);

}

const fixtures = require('test/fixtures');

const {test_users} = fixtures;

let i = 0;

// hypothetical auth point. simply assign a user object to the session
app.get('/auth', (req, res, next)=> {

  // really just testing if the role/permission checking middleware works
  // this middleware is not about handshakes or authentication. Each time
  // a request is made here, we will switch sessions to a different 
  // user
  req.session.current_user = test_users[i++]; 

  res.status(200).json({logged_in: true});
  
});

const {test_routes} = fixtures;

test_routes.forEach((route, index) => {

  const {url, allowed_params} = route;

  app.get(url, [
    allowed(allowed_params),
    // could be anything here, not important
    log_result
  ]);

});

const print_results = () => {

  for (let i in results)
    console.log(results[i]);

};

// TODO: inject argv for port
const test_port = 3000;

app.listen(test_port, '127.0.0.1', () => {

  console.log([
    'test server started for "allowed" middleware.',
    'listening on port', test_port, 'beep boop' 
  ].join(' '));

});

module.exports = print_results;
